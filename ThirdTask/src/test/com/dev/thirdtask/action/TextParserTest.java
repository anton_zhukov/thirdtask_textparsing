package test.com.dev.thirdtask.action;

import com.dev.thirdtask.action.TextParser;
import com.dev.thirdtask.action.TextReader;
import com.dev.thirdtask.entity.CompositeItem;
import com.dev.thirdtask.utils.WholeTextModification;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.List;


public class TextParserTest {
    private static final Logger LOG = Logger.getLogger(TextParserTest.class);
    private String path = "text\\text2.txt";
    private static final String REGEX_SPLIT = "([\\s\\p{Punct}])+";
    private static TextParser textParser;

    @BeforeClass
    public static void initTextParser() {
        textParser = new TextParser();
    }

    @AfterClass
    public static void clearTextParser() {
        textParser = null;
    }

    @Test
    public void initReadingTest() {
        BufferedReader reader = null;
        boolean success = false;

        try {
            String actual = TextReader.initReading(path);
            StringBuilder expected = new StringBuilder();
            reader = new BufferedReader(new FileReader(path));
            String tmp;
            while ((tmp = reader.readLine()) != null) {
                expected.append(tmp).append("\n");
            }
            success = actual.equals(expected.toString());
        } catch (IOException ex) {
            LOG.fatal("IOException", ex);
            throw new RuntimeException(ex);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException exp) {
                    LOG.error("IOException", exp);
                }
            }
        }
        Assert.assertTrue(success);
    }

    @Test
    public void takeSortedSentencesTest() {
        boolean success = true;
        List<CompositeItem> actualResult = WholeTextModification.takeSortedSentences(textParser.parse(path));
        for (int i = 0; i < actualResult.size() - 1; i++) {
            String sentenceOne = actualResult.get(i).toString();
            String sentenceTwo = actualResult.get(i + 1).toString();
            String[] stringsOne = sentenceOne.split(REGEX_SPLIT);
            String[] stringsTwo = sentenceTwo.split(REGEX_SPLIT);
            if (stringsOne.length > stringsTwo.length) {
                success = false;
                break;
            }
        }
        Assert.assertTrue(success);
    }
}
