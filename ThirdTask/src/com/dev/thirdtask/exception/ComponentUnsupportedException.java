package com.dev.thirdtask.exception;


public class ComponentUnsupportedException extends Exception {
    public ComponentUnsupportedException() {
    }

    public ComponentUnsupportedException(String message) {
        super(message);
    }

    public ComponentUnsupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public ComponentUnsupportedException(Throwable cause) {
        super(cause);
    }
}
