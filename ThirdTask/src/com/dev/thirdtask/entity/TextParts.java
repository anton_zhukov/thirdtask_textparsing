package com.dev.thirdtask.entity;

public enum TextParts {
    SIGN, LISTING, LEXEME, WORD, SENTENCE, PARAGRAPH, WHOLE_TEXT
}
