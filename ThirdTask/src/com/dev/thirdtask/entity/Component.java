package com.dev.thirdtask.entity;

import com.dev.thirdtask.exception.ComponentUnsupportedException;


public interface Component {

    boolean addElement(Component component);

    boolean removeElement(Component component);

    TextParts getTextParts();

    Component getElement(int index) throws ComponentUnsupportedException;
}
