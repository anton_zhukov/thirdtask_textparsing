package com.dev.thirdtask.entity;

import com.dev.thirdtask.exception.ComponentUnsupportedException;

public class LeafItem implements Component {

    private String value;
    private TextParts textParts;

    public LeafItem(String value, TextParts t) {
        this.value = value;
        textParts = t;
    }

    public String getValue() {
        return value;
    }

    @Override
    public boolean addElement(Component component) {
        return false;
    }

    @Override
    public boolean removeElement(Component component) {
        return false;
    }

    @Override
    public Component getElement(int index) throws ComponentUnsupportedException {
        throw new ComponentUnsupportedException();
    }

    @Override
    public TextParts getTextParts() {
        return textParts;
    }

    @Override
    public String toString() {
        return value + " ";
    }
}

