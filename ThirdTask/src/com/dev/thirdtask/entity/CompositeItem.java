package com.dev.thirdtask.entity;

import java.util.ArrayList;
import java.util.List;

public class CompositeItem implements Component {

    private TextParts textParts;
    private List<Component> components;

    public CompositeItem(TextParts t) {
        textParts = t;
        this.components = new ArrayList<>();
    }

    @Override
    public boolean addElement(Component component) {
        return components.add(component);
    }

    public List<Component> getComponents() {
        return components;
    }

    @Override
    public boolean removeElement(Component component) {
        return components.remove(component);
    }

    @Override
    public Component getElement(int index) {
        return components.get(index);
    }

    @Override
    public TextParts getTextParts() {
        return textParts;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (Component component : components) {
            builder.append(component);
        }
        return builder.toString();
    }
}
