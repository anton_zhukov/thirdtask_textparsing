package com.dev.thirdtask.action;

import com.dev.thirdtask.entity.CompositeItem;
import com.dev.thirdtask.utils.WholeTextModification;
import org.apache.log4j.Logger;


public class Demonstration {

    private static final Logger LOG = Logger.getLogger(Demonstration.class);
    private static final String PATH = "text\\text2.txt";

    public static void demonstrate() {
        TextParser parser = new TextParser();
        CompositeItem wholeText = parser.parse(PATH);

        final int WORD_LENGTH = 7;
        WholeTextModification.deleteDefiniteWord(wholeText, WORD_LENGTH);
        WholeTextModification.takeSortedSentences(wholeText).forEach(LOG::info);
    }
}
