package com.dev.thirdtask.action;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TextReader {
    private static final Logger LOG = Logger.getLogger(TextReader.class);

    public static String initReading(String path) {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(path));
            String temp;
            while ((temp = reader.readLine()) != null) {
                stringBuilder.append(temp).append("\n");
            }
        } catch (IOException e) {
            LOG.fatal("IOException", e);
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException exp) {
                    LOG.error("IOException", exp);
                }
            }
        }
        return stringBuilder.toString();
    }
}
