package com.dev.thirdtask.action;

import com.dev.thirdtask.entity.CompositeItem;
import com.dev.thirdtask.entity.LeafItem;
import com.dev.thirdtask.entity.TextParts;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser {

    public static final String REGEX_LISTING = "((Start listing)[^\t]+(End listing))";
    public static final String REGEX_PARAGRAPH_WITH_LISTING = "\\t+([^\t]+)[^\\t]+|(Start listing)([^\t]+)(End listing)";
    public static final String REGEX_SENTENCE = "(\\s?[\\p{L}\\s\\,\\-]+[.?!])";
    public static final String REGEX_WORD_WITH_SIGN_OR_NOT = "[^\\.,!\\?:;@\\s]*([\\.,!\\?:;@[^\\s]]{1,2})";
    public static final String REGEX_WORD_ONLY = "([^\\.,!\\?:;@]*)";
    public static final String REGEX_WORD_AND_SIGN = "([\\p{L}]+)(\\p{P}{1})";

    public static final String EMPTY_STRING = "";

    private static final Pattern PATTERN_PARAGRAPH = Pattern.compile(REGEX_PARAGRAPH_WITH_LISTING);
    private static final Pattern PATTERN_LISTING = Pattern.compile(REGEX_LISTING);
    private static final Pattern PATTERN_SENTENCE = Pattern.compile(REGEX_SENTENCE);
    private static final Pattern PATTERN_WORD_WITH_SIGN_OR_NOT = Pattern.compile(REGEX_WORD_WITH_SIGN_OR_NOT);
    private static final Pattern PATTERN_WORD_ONLY = Pattern.compile(REGEX_WORD_ONLY);
    private static final Pattern PATTERN_WORD_AND_SIGN = Pattern.compile(REGEX_WORD_AND_SIGN);

    public CompositeItem parse(String path) {
        String text = TextReader.initReading(path);
        CompositeItem wholeText = new CompositeItem(TextParts.WHOLE_TEXT);
        wholeText = parseToParagraph(wholeText, text);
        return wholeText;
    }

    private CompositeItem parseToParagraph(CompositeItem wholeText, String text) {
        CompositeItem paragraphList = new CompositeItem(TextParts.PARAGRAPH);

        Matcher matcher = PATTERN_PARAGRAPH.matcher(text);
        while (matcher.find()) {
            String paragraph = matcher.group();
            Matcher listingMatcher = PATTERN_LISTING.matcher(paragraph);
            if (listingMatcher.find()) {
                String listing = listingMatcher.group();
                LeafItem paragraphLeaf = new LeafItem(listing, TextParts.LISTING);
                paragraph = paragraph.replaceAll(REGEX_LISTING, EMPTY_STRING);
                paragraphList = parseToSentence(paragraphList, paragraph);
                paragraphList.addElement(paragraphLeaf);
            } else {
                paragraphList = parseToSentence(paragraphList, paragraph);
            }
        }
        wholeText.addElement(paragraphList);
        return wholeText;
    }

    private CompositeItem parseToSentence(CompositeItem paragraphList, String paragraph) {
        CompositeItem sentenceList = new CompositeItem(TextParts.SENTENCE);

        Matcher sentenceMatcher = PATTERN_SENTENCE.matcher(paragraph);
        while (sentenceMatcher.find()) {
            String sentence = sentenceMatcher.group();
            sentenceList = parseToWord(sentenceList, sentence);
        }
        paragraphList.addElement(sentenceList);
        return paragraphList;
    }

    private CompositeItem parseToWord(CompositeItem sentenceList, String sentence) {
        CompositeItem wordList = new CompositeItem(TextParts.WORD);

        Matcher wordWithSignOrNot = PATTERN_WORD_WITH_SIGN_OR_NOT.matcher(sentence);
        while (wordWithSignOrNot.find()) {
            String word = wordWithSignOrNot.group();
            Matcher wordOnly = PATTERN_WORD_ONLY.matcher(word);
            if (wordOnly.matches()) {
                LeafItem leafItem = new LeafItem(word, TextParts.WORD);
                wordList.addElement(leafItem);
            } else {
                wordList = parseToLexeme(wordList, word);
            }
        }
        sentenceList.addElement(wordList);
        return sentenceList;
    }

    private CompositeItem parseToLexeme(CompositeItem wordList, String word) {
        CompositeItem wordSignList = new CompositeItem(TextParts.LEXEME);

        Matcher wordAndSign = PATTERN_WORD_AND_SIGN.matcher(word);
        while (wordAndSign.find()) {
            String wordFromLexeme = wordAndSign.group(1);
            String signFromLexeme = wordAndSign.group(2);
            LeafItem wordItem = new LeafItem(wordFromLexeme, TextParts.WORD);
            LeafItem signItem = new LeafItem(signFromLexeme, TextParts.SIGN);
            wordSignList.addElement(wordItem);
            wordSignList.addElement(signItem);
        }
        wordList.addElement(wordSignList);
        return wordList;
    }
}
