package com.dev.thirdtask.utils;

import com.dev.thirdtask.entity.Component;
import com.dev.thirdtask.entity.CompositeItem;
import com.dev.thirdtask.entity.LeafItem;
import com.dev.thirdtask.entity.TextParts;

import java.util.*;
import java.util.regex.Pattern;

public class WholeTextModification {
    private static final Pattern DELETE_PATTERN = Pattern.compile("[А-Яа-я&&[^АаЕеЁёИиЙйОоУуъьЭэЮюЯяЫы]]{1}[а-я]+");

    //Method for deleting word with definite length, that starts with consonant
    public static void deleteDefiniteWord(CompositeItem item, int wordLength) {
        for (Component component : item.getComponents()) {
            if (component.getTextParts() != TextParts.LISTING) {
                CompositeItem currentItem = (CompositeItem) component;
                if (currentItem.getTextParts() != TextParts.WORD) {
                    deleteDefiniteWord(currentItem, wordLength);
                } else {
                    deleteWord(currentItem, wordLength);
                }
            }
        }
    }

    private static void deleteWord(CompositeItem wordsLayer, int wordLength) {
        Iterator<Component> iterator = wordsLayer.getComponents().iterator();
        while (iterator.hasNext()) {
            Component wordComponent = iterator.next();
            if (wordComponent instanceof LeafItem) {
                if (checkForDeleteCondition((LeafItem) wordComponent, wordLength)) {
                    iterator.remove();
                }
            } else {
                CompositeItem lexeme = (CompositeItem) wordComponent;
                Iterator<Component> lexemeIterator = lexeme.getComponents().iterator();
                LeafItem word = (LeafItem) lexemeIterator.next();
                if (checkForDeleteCondition(word, wordLength)) {
                    lexemeIterator.remove();
                }
            }

        }
    }

    private static boolean checkForDeleteCondition(LeafItem leafItem, int wordLength) {
        return DELETE_PATTERN.matcher(leafItem.getValue()).matches()
                && leafItem.getValue().length() == wordLength;
    }

    //Method for printing all the sentences of the text after ascending sort by the amount of words
    public static List<CompositeItem> takeSortedSentences(CompositeItem item) {
        List<CompositeItem> sentencesList = new ArrayList<>();
        takeSortedSentences(item, sentencesList);
        Collections.sort(sentencesList, (o1, o2) -> wordCount(o1) - wordCount(o2));
        return sentencesList;
    }

    private static void takeSortedSentences(CompositeItem item, List<CompositeItem> list) {
        for (Component component : item.getComponents()) {
            if (component.getTextParts() != TextParts.LISTING) {
                CompositeItem composite = (CompositeItem) component;
                if (component.getTextParts() != TextParts.SENTENCE) {
                    takeSortedSentences(composite, list);
                } else {
                    for (Component sentence : composite.getComponents()) {
                        list.add((CompositeItem) sentence);
                    }
                }
            }
        }
    }

    private static int wordCount(CompositeItem item) {
        return wordCount(item, 0);
    }

    private static int wordCount(CompositeItem item, int currentCount) {
        for (Component currentItem : item.getComponents()) {
            if (item.getTextParts() == TextParts.WORD) {
                currentCount++;
            } else {
                currentCount += wordCount((CompositeItem) currentItem, currentCount);
            }
        }
        return currentCount;
    }
}
